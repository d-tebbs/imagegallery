class GalleryPhoto < ApplicationRecord
  belongs_to :photo
  belongs_to :gallery
end
