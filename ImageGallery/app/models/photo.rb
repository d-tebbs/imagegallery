class Photo < ApplicationRecord
  has_one_attached :image, :dependent => :destroy
  has_many :galleries_photos
  has_many :galleries, through: :galleries_photos
end
