class Gallery < ApplicationRecord
  has_many :galleries_photos
  has_many :photos, through: :galleries_photos
  validates :title, :linked_photo_ids, presence: true
end
