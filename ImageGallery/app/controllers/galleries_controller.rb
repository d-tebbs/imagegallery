class GalleriesController < ApplicationController
  before_action :set_gallery, only: %i[ show edit update destroy ]
  before_action :authenticate_user!, except: [:index, :show, :slideshow]

  # GET /galleries or /galleries.json
  # GET /index?order=[title_asc, title_desc, users_asc]
  def index
    @galleries = Gallery.all    
    @users = []
    @galleries.each do |user|
      @users << User.find(user.user_id)
    end
    if params[:order] == 'title_asc'
      @galleries = @galleries.order('title ASC')
    elsif params[:order] == 'title_desc'
      @galleries = @galleries.order('title DESC')
    elsif params[:order] == 'users'
      @galleries = @galleries.order('user_id ASC')
    end

  end

  # GET /galleries/1 or /galleries/1.json
  def show
    @list = (Gallery.find(params[:id])).linked_photo_ids
    @photos = Photo.find(@list)
  end

  # GET /galleries/new
  def new
    @gallery = Gallery.new
    @all_photos = Photo.where("user_id = #{current_user.id}")
  end

  # GET /galleries/1/edit
  def edit
  end

  # GET /galleries/1/slideshow
  def slideshow
    @gallery_user_id = Gallery.find(params[:id]).user_id
    @list = (Gallery.find(params[:id])).linked_photo_ids
    @photos = Photo.find(@list)
  end

  # POST /galleries or /galleries.json
  def create
    @gallery = Gallery.new(gallery_params)
    @gallery.user_id = current_user.id
    
    respond_to do |format|
      if @gallery.save
        format.html { redirect_to @gallery, notice: "Gallery was successfully created." }
        format.json { render :show, status: :created, location: @gallery }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @gallery.errors, status: :unprocessable_entity }
      end
    end
    
  end

  # PATCH/PUT /galleries/1 or /galleries/1.json
  def update
    respond_to do |format|
      if @gallery.update(gallery_params)
        format.html { redirect_to @gallery, notice: "Gallery was successfully updated." }
        format.json { render :show, status: :ok, location: @gallery }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @gallery.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /galleries/1 or /galleries/1.json
  def destroy
    @gallery.destroy
    respond_to do |format|
      format.html { redirect_to galleries_url, notice: "Gallery was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gallery
      @gallery = Gallery.find(params[:id])
      @gallery_id = params[:id]
      @gallery_user_id = Gallery.find(params[:id]).user_id
      @photos = Photo.where("user_id = #{@gallery_user_id}")
      if user_signed_in?
        @all_photos = Photo.where("user_id = #{current_user.id}")
      end
    end

    # Only allow a list of trusted parameters through.
    def gallery_params
      params.require(:gallery).permit(:user_id, :title, :linked_photo_ids => [])
    end
end
