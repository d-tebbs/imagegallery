json.extract! gallery, :id, :user_id, :title, :created_at, :updated_at
json.url gallery_url(gallery, format: :json)
