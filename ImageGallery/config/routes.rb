Rails.application.routes.draw do
  devise_for :users
  resources :photos
  resources :galleries
  resources :errors
  match "galleries/:id/slideshow", to: 'galleries#slideshow', via: [:get]
  root to: 'galleries#index'
  match "/404", to: "errors#not_found", via: :all
  match "/500", to: "errors#internal_server_error", via: :all
end
