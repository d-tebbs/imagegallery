# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# User.where(email:'testEmail@gmail.com').first_or_create(password:'Password.123', password_confirmation: 'Password.123')
# User.where(email:'otherEmail@gmail.com').first_or_create(password:'Password.123', password_confirmation: 'Password.123')
3.times do |i|
  User.create(email:"test#{i}@gmail.com", password:'Password.123', password_confirmation:'Password.123') 
end