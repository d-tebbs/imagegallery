class CreateJoinTablePhotoGallery < ActiveRecord::Migration[6.0]
  def change
    create_join_table :photos, :galleries do |t|
      t.index [:photo_id, :gallery_id]
      t.index [:gallery_id, :photo_id]
    end
  end
end
