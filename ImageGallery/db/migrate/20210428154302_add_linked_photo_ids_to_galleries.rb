class AddLinkedPhotoIdsToGalleries < ActiveRecord::Migration[6.0]
  def change
    add_column :galleries, :linked_photo_ids, :string, array: true, default: []
  end
end
