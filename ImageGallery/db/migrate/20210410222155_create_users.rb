class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.integer :user_id
      t.string :email
      t.string :password_hash
      t.string :given_name
      t.string :family_name

      t.timestamps
    end
  end
end
