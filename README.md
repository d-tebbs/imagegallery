# The Curve, Image Gallery Technical Test:
## Cloning and Running the project:
Start by cloning into the repo:
```
$ git clone git@gitlab.com:d-tebbs/imagegallery.git
```
Then, starting in the project root:
```
$ cd ImageGallery
$ yarn
$ sudo service postgresql start
$ bundle exec rails db:create
$ bundle exec rails db:migrate:reset
$ bundle exec rails db:seed
$ bundle exec rails s
```
Then visit http://localhost:3000/ and you should be greeted by the homepage.
I've included that you run `Yarn` and `migrate:reset` as without these when I tried these setup steps there were commonly errors, your system may differ?
#
## Versions:
I have been using Ruby v2.6.6 and Rails v6.0.3.6 as those are the versions that I have installed on my Linux dist. for my Uni projects.

Yarn is v1.22.5 and Node is v15.12.0

## Docker:
Unfortunately I am completely unfamiliar with Docker/Docker Compose, and although I did try to understand it for this project, I couldn't get it to install properly I think? I'm still unsure why it didn't work.

## Accounts and default logins:
The seed file contains 3 accounts, `test0@gmail.com`, `test1@gmail.com` and `test2@gmail.com`. The passwords for all 3 are the same: `Password.123`

This is insecure, but these are just example accounts. 

There are no default galleries or images.
